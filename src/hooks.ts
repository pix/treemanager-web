import * as cookie from 'cookie'
import type { ResolveOptions, RequestEvent } from "@sveltejs/kit"
import type { User } from '$lib/types'
import { fetchMe } from '$lib/api'
const protectedPages = ["/user",];

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve }) {
  const cookies = event.request.headers.get("cookie") || ""
  let loggingOut = event.url.pathname === "/auth/logout"

  let response: Response;

  if (loggingOut) {
    event.locals.user = null
    response = new Response(null, {
      status: 302,
      headers: [
        ["Set-Cookie", "sessionid=deleted; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT"],
        ["location", "/"]
      ]
    })
    return response
  } else {
    const user: User = await fetchMe(cookies)
    event.locals.user = user

    response = await resolve(event);
    return response
  }
}

//export function getSession(event) {
//  const { user } = event.locals;
//  return {
//    user: user,
//  };
//}
