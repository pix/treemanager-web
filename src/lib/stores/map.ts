import type { Tree } from "$lib/types"
import type { Map } from "leaflet"
import { writable, type Writable } from "svelte/store"

export const MapStore: Writable<Map | null> = writable()
export const SelectedTreeStore: Writable<Tree | null> = writable()
