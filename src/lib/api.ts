import type { User } from "./types"
import * as cookie from "cookie";
import { UserStore } from "$lib/stores/user"
import { API_BASE_URL } from "./constants";


export async function fetchMe(cookies: string) {
    let req: Request = new Request(`${API_BASE_URL}/auth/me/`, {
        credentials: "include",
        headers: [
            ["Content-type", "application/json;"],
            ["Cookie", cookies || ""]
        ]
    })

    let resp = await fetch(req)
    let user: User = { authenticated: false }
    user = await resp.json()

    return user
}

export async function fetchLogin(form: HTMLFormElement) {
    const resp = await fetch(`${API_BASE_URL}/auth/login/`, {
        method: "POST",
        credentials: "include",
        headers: [
            ["Accept", "application/json;"],
            ["X-CSRFToken", cookie.parse(document.cookie)["csrftoken"]]
        ],
        body: new FormData(form)
    })

    return resp
}
