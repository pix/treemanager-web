import type { Feature, Point } from "geojson";
import { intros } from "svelte/internal";

export type User = {
    authenticated: boolean
    user?: {
        id: number,
        username: string,
        groups: string[]
    }
}

export interface Key {
    key: string;
    tag_type: string;
}

export interface Tag {
    key: Key,
    value: any;
}

export interface Change {
    id?: number
    tree?: number
    edited_at: string,
    tags: Tag[]
}

export interface Tree extends Feature {
    id?: number
    properties: {
        name: string;
        species: string;
        changes: Change[]
    };
    geometry: Point
}

export interface TreeFormErrors {
    name?: string[]
    species?: string[]
    changes?: DatapointFormErrors[]
}

export interface DatapointFormErrors {
    edited_at?: string[]
    tags?: TagFormError[]
}

export interface TagFormError {
    key?: {
        key?: string[],
        tag_value?: string[]
    },
    value?: string[]
}
