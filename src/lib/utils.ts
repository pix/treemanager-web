import type { Tag } from "$lib/types"

export function getCookie(cookieString: string | null, name: string) {
    let cookieValue: string = "";
    if (cookieString && cookieString !== '') {
        const cookies = cookieString.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


export function emptyTag(): Tag {
    return { key: { key: "", tag_type: "" }, value: null }
}
