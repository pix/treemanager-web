import { API_BASE_URL } from "$lib/constants";
import type { RequestEvent } from "@sveltejs/kit";

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function GET(event: RequestEvent) {
    const cookies: string | null = await event.request.headers.get("cookie")
    
    let req: Request = new Request(`${API_BASE_URL}/api/keys/`, {
        credentials: "include",
        headers: [
            ["Content-type", "application/json;"],
            ["Cookie", cookies || ""]
        ]
    })

    return await fetch(req)
} 