import { API_BASE_URL } from "$lib/constants";
import { getCookie } from "$lib/utils";
import type { RequestEvent } from "@sveltejs/kit";

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function POST(event: RequestEvent) {
    const cookies: string | null = await event.request.headers.get("cookie")
    const csrftoken = getCookie(cookies, "csrftoken")
    const datapoint = JSON.stringify(await event.request.json())

    let req: Request = new Request(`${API_BASE_URL}/api/datapoints/create/`, {
        method: "POST",
        credentials: "include",
        headers: [
            ["Content-type", "application/json;"],
            ["Cookie", cookies || ""],
            ["X-CSRFToken", csrftoken]
        ],
        body: datapoint
    })

    return await fetch(req)
} 