import { UserStore } from "$lib/stores/user";
import type { RequestEvent } from "@sveltejs/kit";

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function GET(event: RequestEvent) {
    UserStore.update((u) => u = null)

    return {
        status: 302,
        headers: {
            location: "/"
        }
    }
} 