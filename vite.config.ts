import { sveltekit } from '@sveltejs/kit/vite';
import { resolve } from "path";

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [sveltekit()],

	resolve: {
		alias: {
			'~bootstrap': resolve(__dirname, 'node_modules/bootstrap'),
		}
	},
	ssr: {
		noExternal: ['chart.js']
	},
	optimizeDeps: {
		include: ['chart.js']
	},
};

export default config;